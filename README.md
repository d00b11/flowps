# FloWPS user’s manual

FloWPS (FLOating Window Projective Separator) is an approach that improves the performance of SVM-based classifiers. Such improvement is done via data trimming before the construction of SVM models. This data trimming is specific to any validation point.    

## Executable scripts

The deposited scripts were designed for leave-out-out cross-validation of our approach. For any validation point, predictions of FloWPS classifier are calculated using `prediction.R`. The script `prediction.R` uses as input data the SVM predictions calculated by the script `flowps.py`, for various values of data trimming parameters. According to leave-one-out methodology, both SVM calculations with different data trimming parameters in `flowps.py` and further optimization of trimming parameters by `prediction.R` are done for any distinct point that is considered the validation point, whereas the resting points serve as training dataset. 

## Prerequisites 

`flowps.py`: listed in `requirements.txt` and can be installed via standard `pip`:

```
pip install -r requirements.txt
```

`prediction.R` requires `caTools` package:

```
install.packages("caTools")
```

## Input data structure 

### CSV file:

* First column: sample ID
* Second column: observed classification scores for two (and only two) classes
* The rest columns: features

### Example

`data/TARGET20_with_busulfan.csv`. It is a gene expression dataset with clinical response of individual cancer patients to certain anti-cancer treatment. In that case:

* First column: patient ID
* Second column: response which is 100 for clinical responders or 0 for non-responders
* The rest columns: various gene expressions

Other datasets may be found in the Supplementary Tables 2 and 3 for the paper (Tkachev et al., 2018).

## `flowps.py` : data trimming and SVM predictions 

To calculate the SVM predictions, *Pi*(*m*,*k*), over the desired range of data trimming parameters *m* and *k*, please run `flowps.py` The arguments, which the `flowps.py` depends upon, are described in the `flowps.bash` file, as follows:

``` 
python flowps.py \
    --train-file data/TARGET20_with_busulfan.csv \
    --min-surround 0 --max-surround 10 \
    --min-neighbours 32 --max-neighbours 45
```

* `--train-file` -- classes-and-features CSV file structured similarly to the file `data/TARGET20_with_busulfan.csv`
* `--min-surround`, `--max-surround` -- minimal and maximal values of *m*
* `--min-neighbours`, `--max-neighbours` -- minimal and maximal values of *k*

### Suggestions for *m* and *k* range selection and troubleshooting

* The feature selection parameter *m* specifies the number of surrounding training points along the axis that corresponds to a feature, which is sufficient to keep this feature as relevant for building the SVM model. Therefore, the value *m* = 0 corresponds to no feature selection. The theoretical maximum for *m* value is certainly *(N-1)*/2, where *N* is the number of samples in the dataset, since if *m* > *(N-1)*/2, then no feature would be chosen as relevant. In practice, there is a chance to encounter an error during the execution of the `flowps.py` script due to no relevant feature survived even when *m* < *(N-1)*/2, particularly for the datasets with relatively low number of features. In this case, please do not panic, and simply decrease the argument for maximal number of *m*, `max-surround`. 

* The *k* parameter specifies the number of nearest neighbors in the subspace of selected features. Although this approach seems quite similar to the kNN machine learning method, unlike the kNN, where *k* is relatively small and does not usually exceed 20, we recommend to make *k* values for FloWPS higher. The maximal possible number for *k* is clearly *N*-1, which correspond to no training point selection, whereas at relatively low values there is a risk of another SVM error, due to presence of only single-class training points among the *k* nearest neighbors. This trouble can also be easily fixed by increasing the  `min-neighbours` argument.  

* It is recommended to always keep the values of `min-surround`=0 and `max-neighbours` = *N*-1 since it never produces an error during the `flopws.py` execution, and provides a reference for comparison of FloWPS with classical SVM without data trimming, which corresponds to *m* = 0, *k* =*N*-1. 

  ### Suggestions for user-made modification of the code

In the `flowps.py` code, the user can adjust the cost/penalty parameter for SVM (*C*), or the kernel type, as follows:

```
self.clf = svm.LinearSVC(C=1)
```

or 

```
self.clf = svm.SVC(kernel='poly', C=1)
```

### Expected computation time

Please note that execution of `flowps.py` may be time-consuming due to wide ranges of *m* and *k* and a large number *N* of samples in the dataset. At a typical contemporary computer, e.g. with 31 Gb of RAM and 8*4.20 GHz CPUs, running the `flowps.py` script for our smallest cancer dataset , `TARGET20_with_busilfan`, which has 46 samples, takes about 20 seconds, whereas our biggest cancer dataset, `GSE25066`, which has 235 samples, will require a few hours to be calculated.  So, if the user intends to process a dataset, which is considerably larger than our cancer datasets, he/she should be warned about the computation time. Fortunately, the expected computation time can be easily assessed using the running log through the screen during the execution of `flopws.py`.  

## Output data of `flowps.py` 

Running the `flowps.py` produces the results in the subdirectory `results/<dataset>`, namely:

* `predictions.csv` – SVM predictions, *Pi*(*m*,*k*), as a function of data trimming parameters, *m* and *k*, for all validation points. Each row corresponds to distinct values of *m* and *k* parameters, and each column corresponds to each sample. 

* `auc.png` – AUC(*m*, *k*) color topogram, that shows the dependence of the ROC AUC metric for the SVM classifier upon the data trimming parameters, *m* and *k*, similar to Fig. 2 B in the paper (Tkachev et al, 2018).

## `prediction.R`: calculating FloWPS predictions

Finding the prediction-accountable sets for each sample and calculation of overall FloWPS predictions can be done using `prediction.R`. 

As the input data, is uses the initial classes-and-features CSV file structured similarly to the file `data/TARGET20_with_busulfan.csv`, as well as the `predictions.csv` matrix.

### Suggestions for user-made modification of the code 

Please modify the code to set up the following parameters, 

* `min_surround`and `max_surround` specify the range of *m* (should be the same as for the `flowps.py` script), 
* `min_neighbours `and `max_neighbours ` specify the range of *k*  (should be the same as for the `flowps.py` script),
* *p* stands for confidence threshold of prediction-accountable set *Si* for the sample *i*. For this prediction-accountable set, only those (*m*,*k*) pixels of the AUC*i*(*m*,*k*) topogram are selected, where AUC*i*(*m*,*k*) > *p*·max(AUC*i*(*m*,*k*)). Our experience suggests that reasonable values for *p* are 0.90 or 0.95.

## Output data of `prediction.R`

Running this code produces the file `results/<dataset>/y_flowps_score.csv`.

Columns:
* `names`: sample ID
* `y`: observed classification score
* `flowps_score`: overall FloWPS predictions, *PF*. 

## Interpretation of results

* The *PF* values are expressed in regression-like terms, i.e. they are likelihoods for attribution of samples to any of two classes. The discrimination threshold, which the user may apply to distinguish between two classes, should be determined according to the cost balance between false positive and false negative errors. In our cancer datasets example, we considered the costs for false positive and false negative errors to be equal, and then maximized the overall accuracy rate, ACC = (TP+TN)/(TP+TN+FP+FN), since the class sizes in our model datasets were equalized by design.
* As far as it was mentioned before, it may be interesting to compare the performance of FloWPS to classical SVM with no data trimming, so for this purpose one should run `flopws.py` script with *m* = 0, *k* = *N*-1.